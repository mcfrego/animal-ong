const volunteerModel = require('../models/volunteers.model')

function getAllVolunteers(req, res) {
  volunteerModel.find()
    .populate('onChargeAnimals')
    .then((volunteers) => {
      res.json(volunteers)
    })
    .catch((err) => {
      res.json(err)
    })
}

function getVolunteer(req, res) {
  const volunteerId = res.locals.id

  volunteerModel.findById(volunteerId)
    .then((volunteer) => {
      res.json(volunteer)
    })
    .catch((err) => {
      res.json(err)
    })
}

module.exports = {
  getAllVolunteers,
  getVolunteer
}