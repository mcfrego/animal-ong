const mongoose = require('mongoose')

const animalSchema = new mongoose.Schema({
  name: {
    type: String,
  },
  type: {
    type: String,
    enum: ['perro', 'gato', 'serpiente']
  },
  age: {
    type: String
  },
  colors: {
    type: String,
    enum: ['color-liso', 'moteado', 'multicolor']
  }
})

const animalModel = mongoose.model('animal', animalSchema)
module.exports = animalModel